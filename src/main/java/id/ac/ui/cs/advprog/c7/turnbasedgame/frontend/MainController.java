package id.ac.ui.cs.advprog.c7.turnbasedgame.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class MainController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String homepage(Model model) {
        return "homepage";
    }

    @RequestMapping(value = "/initGame", method = RequestMethod.GET)
    public String initGame (Model model) {
        return "game";
    }

}