$(document).ready(function(){
    // Cookie
    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    var playertoken = getCookie("usertoken");
    // Cookie

    // Global Variable
    var currentLevel = 1;
    var selectedEnemy;
    var enemyName;
    var enemyHealth;
    var heroName;
    var heroHealth;
    var heroSkill;
    //

    // Load data
    inisialisasiGetter();

    function inisialisasiGetter(){
        getRound(playertoken);
        getLevelAndEXP(playertoken);
        getHeroStats(playertoken);
        getEnemyStats(playertoken);
        getLogsBackAfterRefresh();
    }
    //

    function getRound(token){
        var endpoint = "https://c7-game-round-playerlevel.herokuapp.com/v1/round/findRoundBy/"+ token;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                var htmlClassRound = ".title-text#round";
                $(htmlClassRound).empty();

                var round = data;

                var currentRound = round.currentRound;

                var round =
                'ROUND: ' + currentRound;

                $(htmlClassRound).append(round);
            }
        });
    }

    function getLevelAndEXP(token){
        var endpoint = "https://c7-game-round-playerlevel.herokuapp.com/v1/level/findLevelBy/" + token ;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                var htmlClassLevel = ".title-text#level";
                var htmlClassExperience = ".title-text#experience";
                $(htmlClassLevel).empty();
                $(htmlClassExperience).empty();

                var playerLevel = data;
                var currentExperience = playerLevel.experience;
                var currentPlayerLevel = playerLevel.currentLevel;

                var exp = 'EXP: ' + currentExperience;
                var level = 'LEVEL: ' + currentPlayerLevel;

                $(htmlClassLevel).append(level);
                $(htmlClassExperience).append(exp);
                console.log("++++++++", currentPlayerLevel, "+++++++++", currentLevel)
                if (currentPlayerLevel > currentLevel) {
                    currentLevel = currentPlayerLevel
                    levelUpHeroes(token);
                }
            }
        });
    }

    function getHeroStats(token){
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token + "/heroes" ;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                console.log("GetHeroesStats");
                var heroes = data;
                for (var i=0; i<heroes.length; i++){
                    var hero = heroes[i];
                    var namaHero = hero.name;
                    var htmlClassDeskripsiHero = '#' + namaHero.toLowerCase() + ".deskripsi-character";
                    var htmlClassHeroCard = '#' + namaHero.toLowerCase() + "-card.card.hero";
                    $(htmlClassDeskripsiHero).empty();

                    var currentHealth = hero.currentHealth;
                    var currentSkillUsage = hero.currentSkillUsage;
                    var attack = hero.attack;

                    $(htmlClassHeroCard).attr("value", namaHero + " " + currentHealth + " " + currentSkillUsage);

                    var stats =
                    '<p> HP: ' + currentHealth + '</p>' +
                    '<p> Attack: ' + attack + '</p>' +
                    '<p> SkillUsage: ' + currentSkillUsage + '</p>' ;

                    $(htmlClassDeskripsiHero).append(stats);
                }
            }
        });
    }

    function getEnemyStats(token){
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token + "/enemies" ;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                console.log("GetEnemyStats");
                var enemies = data;
                for (var i=0; i<enemies.length; i++){
                    var enemy = enemies[i];
                    var namaEnemy = enemy.name;
                    var htmlClassDeskripsiEnemy = '#' + namaEnemy.toLowerCase() + ".deskripsi-character";
                    var htmlClassEnemyCard = '#' + namaEnemy.toLowerCase() + "-card.card.enemy";
                    $(htmlClassDeskripsiEnemy).empty();

                    var currentHealth = enemy.currentHealth;
                    var currentSkillUsage = enemy.currentSkillUsage;
                    var attack = enemy.attack;

                    $(htmlClassEnemyCard).attr("value", namaEnemy + " " + currentHealth + " " + currentSkillUsage);

                    var stats =
                    '<p> HP: ' + currentHealth + '</p>' +
                    '<p> Attack: ' + attack + '</p>' +
                    '<p> SkillUsage: ' + currentSkillUsage + '</p>' ;

                    $(htmlClassDeskripsiEnemy).append(stats);
                }
            }
        });
    }

    function getLogsBackAfterRefresh(){
        var log = getCookie("logs");
        var htmlClassLogs = ".log-character";
        $(htmlClassLogs).append(log);
    }

    function levelUpHeroes(token){
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token + "/level-up" ;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                console.log(data);
            }
        });
    }

    function updateLogs(token, log){
        var htmlClassLogs = ".log-character";
        $(htmlClassLogs).append(log + "<br>");
        setCookie("logs", $(htmlClassLogs).html(), 365);
    }

    function attack(token, user, target){
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token + "/" + user + "/attack/" + target ;
        console.log("masukFungsiAttack");
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                var logs = data;
                for (var i=0; i<logs.length; i++){
                    updateLogs(token, logs[i]);
                }
                console.log("berhasilAttack");
                getGameState(playertoken);
                getHeroStats(token);
                getEnemyStats(token);
            }
        });
    }

    function skill(token, user){
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token + "/" + user + "/skill" ;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                var logs = data;
                for (var i=0; i<logs.length; i++){
                    updateLogs(token, logs[i]);
                }
                console.log("berhasil skill");
                getGameState(playertoken);
                getHeroStats(token);
                getEnemyStats(token);
            }
        });
    }

    // Attack dan Skill
    $(".card.hero").click(function(){
        $(".card.hero.active").removeClass("active");
        $(this).addClass("active");
        var valueHero = $(this).attr('value').split(" ");
        heroName = valueHero[0];
        heroHealth = valueHero[1];
        heroSkill = valueHero[2];
        console.log(valueHero);
    });

    $("select").on('change', function() {
        selectedEnemy = this.value;
    });

    $("#attack").click(function(){
        setNewEnemyValue(selectedEnemy);
        console.log(enemyHealth);
        console.log("attackdiKlik");
        if (heroName !== "" && enemyName !== "" && heroHealth !== "0" && enemyHealth !== "0"){
            attack(playertoken, heroName, enemyName);
            $(".card.hero.active").removeClass("active");
            heroName = "";
        } else if (heroHealth === "0") {
            alert("Your hero's health has been zero");
        } else if (enemyHealth === "0") {
            alert(enemyName + " has dead");
        } else {
            alert("Choose your Hero and Enemy to use attack!");
        }
    });

    $("#skill").click(function(){
        console.log("skill diKlik");
        if (heroName !== "" && heroSkill !== "0"){
            skill(playertoken, heroName);
            heroName = "";
            $(".card.hero.active").removeClass("active");
        } else if (heroSkill === "0") {
            alert("Your hero's skill usage has been zero");
        } else {
            alert("Choose your Hero to use skill!");
        }
    });

    function setNewEnemyValue(name){
        var htmlClassEnemyCard = '#' + name.toLowerCase() + "-card.card.enemy";
        var valueEnemy = $(htmlClassEnemyCard).attr('value').split(" ");
        enemyName = valueEnemy[0];
        enemyHealth = valueEnemy[1];
    }
    //  Attack dan Skill


    // Game State
    function getGameState(token){
        console.log("masuk ke getGameState");
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token + "/status";
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            contentType: 'application/json; charset=utf-8',
            complete: function(data){
                var gameState = data.responseText;
                console.log(gameState);
                checkState(token, gameState);
            },
        });
    }

    function updateRound(token, state){
        var endpoint = "https://c7-game-round-playerlevel.herokuapp.com/v1/round/updateRoundBy/" + token + "/basedOn/" + state ;

        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                getRound(token);
            }
        });
    }

    function updateLevelAndEXP(token, state){
        var endpoint = "https://c7-game-round-playerlevel.herokuapp.com/v1/level/updateLevelAndEXPBy/" + token + "/basedOn/" + state ;

        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                getLevelAndEXP(token);
            }
        });
    }

    function checkState(token, state){
        console.log("Masuk ke checkState");
        if (state === 'WIN') {
            console.log("Masuk case win");
            $('#modal-win').modal('show');
            $("#win-modal-home.btn.btn-primary.mb-2").click(function(){
                $('#modal-win').modal('hide');
                forceReset(token);
                window.location.href = "http://c7-game-frontend.herokuapp.com/";
            });
            $("#win-modal-continue.btn.btn-primary.mb-2").click(function(){
                $('#modal-win').modal('hide');
                inisialisasiGetter();
            });
        } else if (state === 'LOSE') {
            console.log("Masuk case lose");
            $('#modal-lose').modal('show');
            $("#lose-modal-home.btn.btn-primary.mb-2").click(function(){
                $('#modal-lose').modal('hide');
                deleteGameSimulator(token);
                window.location.href = "http://c7-game-frontend.herokuapp.com/";
            });
        }
        updateRound(token, state);
        updateLevelAndEXP(token, state);
    }

    function forceReset(token){
        deleteGameSimulator(token);
        updateRound(token, "LOSE");
        updateLevelAndEXP(token, "LOSE");
        console.log("berhasil force")
    }

    function deleteGameSimulator(token){
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token ;
        $.ajax({
            type: 'DELETE',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                console.log("Sukses delete");
            }
        });
    }
});