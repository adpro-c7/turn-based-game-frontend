$(document).ready(function(){

    //  Token and Object Creation
    findHighScore();
    var playertoken = getCookie("usertoken");

    function loadCookie(){
        setCookie("logs", "", 365);
        if (playertoken === ""){
            $('#modal-name').modal('show');
            $('#modal-name-submit').click(function(){

                var user_name = $("#name-input").val();
                var user_token = user_name + Math.random().toString(36).substr(2, 9);
                console.log("getUserName");
                console.log(user_name);

                setCookie("usertoken", user_token, 365);
                playertoken = getCookie("usertoken");
                location.reload();
            });
        }
        createLevel(playertoken);
        createRound(playertoken);
        createGameSimulator(playertoken);
    }

    function setCookie(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
        var expires = "expires="+d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for(var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function findHighScore(){
        var endpoint = "http://c7-game-round-playerlevel.herokuapp.com/v1/round/findHighScore";

        $.ajax({
            type: 'GET',
            url: endpoint,
            success: function(data){
                $("#highscore-text").empty();
                var currentHighscore = data;
                var highScore = "HIGHSCRORE: " + currentHighscore;
                $("#highscore-text").append(highScore);
                console.log(highScore);
                loadCookie();
            }
        });
    }

    function createRound(token){
        var endpoint = "https://c7-game-round-playerlevel.herokuapp.com/v1/round/createRound/" + token ;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                console.log("sukses createRound");
            }
        });
    }

    function createLevel(token){
        var endpoint = "https://c7-game-round-playerlevel.herokuapp.com/v1/level/createLevel/" + token ;
        $.ajax({
            type: 'GET',
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            success: function(data){
                console.log("sukses createLevel");
            }
        });
    }


    function createGameSimulator(token){
        var endpoint = "https://c7-game-gameplay-character.herokuapp.com/v1/gameplay/" + token ;
        console.log("fungsi createGameTerpanggil");
        $.ajax({
            type : "POST",
            url: endpoint,
            headers: {
                "X-CSRFToken": playertoken,
            },
            complete: function(data){
                console.log("sukses createGame");
            }
        });
    }
    //  Token and Object Creation
});
