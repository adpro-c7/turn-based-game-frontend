# Group Project C7
## Advance Programming 2020/2021
[![pipeline status](https://gitlab.com/adpro-c7/turn-based-game-frontend/badges/master/pipeline.svg)](https://gitlab.com/adpro-c7/turn-based-game-frontend/-/commits/master/)
[![coverage report](https://gitlab.com/adpro-c7/turn-based-game-frontend/badges/master/coverage.svg)](https://gitlab.com/adpro-c7/turn-based-game-frontend/-/commits/master/)

# Turn-based Game
Web game where we can fight enemies.

### Repository:
[https://gitlab.com/adpro-c7/turn-based-game-frontend](https://gitlab.com/adpro-c7/turn-based-game-frontend)

### Deployed Site:
[http://c7-game-frontend.herokuapp.com/](http://c7-game-frontend.herokuapp.com/)

### Patterns Implemented:
- Command Pattern
- Observer Pattern
- Strategy Pattern